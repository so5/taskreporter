from secret import * 
import urllib.request
import json
import re
import pprint
import time

wip_list_names=("WIP", "W.I.P.", "W.I.P", "Doing", "doing")
todo_list_names=("ToDo", "todo", "Todo", "To do", "to do")

class TrelloClient:
    def __init__(self, api_key, token):
        self.api_key=api_key
        self.token=token
        self.base_url="https://api.trello.com/1"
    def fetch(self, category, api, target_id, option=""):
        url='{}/{}/{}/{}?key={}&token={}{}'.format(self.base_url,category,target_id,api,self.api_key,self.token,option)
        with  urllib.request.urlopen(url) as r:
            if r.getcode() != 200:
                raise Exception("return {} from {}".format(r.getcode(), r.geturl()))
            return json.loads(r.read().decode('utf-8'))

    def batch_fetch(self, category, api, target_ids, option=""):
        batch_url=""
        for target_id in target_ids:
            batch_url += "/{}/{}/{}/{},".format(category, target_id, api,option)
        batch_url=urllib.parse.quote(batch_url.rstrip(','))
        url="{}/batch/?urls={}&key={}&token={}".format(self.base_url, batch_url, self.api_key, self.token)
        with  urllib.request.urlopen(url) as r:
            return json.loads(r.read().decode('utf-8'))

    
def parse_hours(cards):
    reg1=re.compile(r'\(([0-9]+)\)')
    reg2=re.compile(r'\[([0-9]+)\]')
    for card in cards:
        estimated=reg1.search(card['name'])
        if estimated :
            card['estimated']=estimated.group(1)
            card['name']=reg1.sub("", card['name'])
        consumed=reg2.search(card['name'])
        if consumed :
            card['consumed']=consumed.group(1)
            card['name']=reg2.sub("", card['name'])

def plain_text_reporter(wip, todo):
    report="現在作業中のタスク\n"
    for project, tasks in wip.items():
        parse_hours(tasks)
        report+="  {}: \n".format(project)
        for task in tasks:
            report+="    -{}\n".format(task['name'])
        report+="\n"
    report+="予定されているタスク\n"
    for project, tasks in todo.items():
        parse_hours(tasks)
        report+="  {}: \n".format(project)
        for task in tasks:
            report+="    -{}\n".format(task['name'])
        report+="\n"
    return report


def gather_from_trello():
    tc=TrelloClient(trello_api_key, trello_token)
    boards=tc.fetch(category="member", api="boards", target_id="me", option="&filter=open&fields=name")
    wip={}
    todo={}
    for board in boards:
        lists=tc.fetch(category="boards", target_id=board["id"], api="lists", option="&filter=open&fields=name")
        for list in lists:
            if list['name'] in  todo_list_names:
                cards=tc.fetch(category="lists", target_id=list["id"], api="cards", option="&filter=open&fields=name,shortUrl,due")
                if len(cards) > 0:
                    todo[board["name"]]=cards
            if list["name"] in wip_list_names:
                cards=tc.fetch(category="lists", target_id=list["id"], api="cards", option="&filter=open&fields=name,shortUrl,due")
                if len(cards) > 0:
                    wip[board["name"]]=cards
    return wip,todo

def gather_from_trello_batch():
    tc=TrelloClient(trello_api_key, trello_token)
    boards=tc.fetch(category="member", api="boards", target_id="me", option="&filter=open&fields=name")
    board_ids=[board['id'] for board in boards]
    results=tc.batch_fetch(category="boards", api="lists", target_ids=board_ids, option="?filter=open&fields=name")
    wip_list_ids=[j['id'] for i in results for j in i.get('200') if j['name'] in todo_list_names]
    todo_list_ids=[j['id'] for i in results for j in i.get('200') if j['name'] in wip_list_names]
    list_ids=wip_list_ids+todo_list_ids
    results=tc.batch_fetch(category="lists", api="cards", target_ids=list_ids, option="?filter=open&fields=name&fields=shortUrl&fields=due&fields=idList&fields=idBoard")
    cards=[j for i in results for j in i.get('200')]
    wip={}
    todo={}
    for board in boards:
        tmp_wip=[card for card in cards if card['idList'] in wip_list_ids and card['idBoard'] == board['id']]
        if len(tmp_wip)>0:
            wip[board['name']] =tmp_wip
        tmp_todo=[card for card in cards if card['idList'] in todo_list_ids and card['idBoard'] == board['id']]
        if len(tmp_todo)>0:
            todo[board['name']]=tmp_todo
    return wip, todo
    

if __name__ == "__main__":
    #t0=time.time()
    #wip, todo = gather_from_trello()
    t1=time.time()
    wip, todo = gather_from_trello_batch()
    t2=time.time()
    #print("elapsed time for first  version = {:.2e} sec".format(t1-t0))
    print("elapsed time for second version = {:.2e} sec".format(t2-t1))

    print(plain_text_reporter(wip, todo))


